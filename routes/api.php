<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//list Products
Route::get('/products','ProductsController@index');
//list single element product
Route::get('/product/{id}','ProductsController@show');
//create new product
Route::post('product','ProductsController@store');
//update product
Route::put('/product','ProductsController@update');
//delete product
Route::delete('/product/{id}','ProductsController@destroy');

// Route::Resource('products','productsController');
Route::get('/admins','AdminsController@index');
//list single element product
Route::get('/admin/{id}','AdminsController@show');
//create new product
Route::post('admin','AdminsController@store');
//update product
Route::put('/admin','AdminsController@update');
//delete product
Route::delete('/admin/{id}','AdminsController@destroy');

Route::get('/users','UsersController@index');
//list single element product
Route::get('/user/{id}','UsersController@show');
//create new product
Route::post('user','UsersController@store');
//update product
Route::put('/user','UsersController@update');
//delete product
Route::delete('/user/{id}','UsersController@destroy');