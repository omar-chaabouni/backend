<?php

namespace App\Http\Controllers;
use App\Model\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Requests;
class ProductsController extends Controller
{   
    private $productRepository;
    public function __construct(ProductRepository $productRepository ){
        $this->productRepository = $productRepository;

    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->productRepository->all());      
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has(["name","price","quantity"])){
        return response()->json($this->productRepository->addProduct($request));}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $product=Product::findOrFail($id);
        // return $product;
        return response()->json($this->productRepository->findById($id));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->productRepository->findById($id)->exists){
        $this->productRepository->delete($id);}
        // $products=Product::All();
        // $product=Product::findOrFail($id);
        // if($product->delete()) return response()->json($products);
    }
}
