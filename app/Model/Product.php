<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
    use SoftDeletes;
    protected $table='Product';
    protected $primaryKey='id';
    public $timestamps=true;
    protected $fillable = ['name', 'price', 'quantity', 'price','description'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    
}
