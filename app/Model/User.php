<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Model
{
    use SoftDeletes;
    protected $table='User';
    public $primaryKey='id';
    protected $fillable = ['name', 'email', 'passwrod'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    public $timestamps = true;
}
