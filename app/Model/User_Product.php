<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class User_Product extends Model
{
    use SoftDeletes;
    protected $table='User_Product';
    public $primaryKey='id';
    protected $fillable = ['product_id', 'user_id','quantity_purchased'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    public $timestamps = true;
}
