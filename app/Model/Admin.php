<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Admin extends Model
{
    use SoftDeletes;
    protected $table='Admin';
    protected $primaryKey='id';
    protected $fillable ='password';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    public $timestamps = true;
}
